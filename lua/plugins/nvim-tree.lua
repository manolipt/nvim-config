return {
	"nvim-tree/nvim-tree.lua",
	version = "*",
	dependencies = {
		"nvim-tree/nvim-web-devicons",
	},
	lazy = false,
	config = function()
		require("nvim-tree").setup {
		}
	end,
	keys = {
		{ "<leader>e", "<cmd>NvimTreeToggle<cr>", desc="Toggle nvim-tree" },
	},
}
