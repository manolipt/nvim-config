return {
	"epwalsh/obsidian.nvim",
	version = "*",  -- recommended, use latest release instead of latest commit
	lazy = true,
	ft = "markdown",
	dependencies = {
		"nvim-lua/plenary.nvim",
		"hrsh7th/nvim-cmp",
		"nvim-telescope/telescope.nvim",
		"nvim-treesitter/nvim-treesitter",
	},

	opts = {
		workspaces = {
			{
				name = "ThoughtPool",
				path = "/home/manolipt/Documents/ThoughtPool",
			},
		},

		daily_notes = {
			folder = "Daily",
			date_format = "%Y/%m/%y-%m-%d %dddd",
			template = "Templates/Daily Note"
		},
		
		completion = {
			nvim_cmp = true,
			min_chars = 2,
		},

		templates = {
			subdir = "Templates",
			date_format = "%Y-%m-%d",
			time_format = "%H:%M",
		},

		open_app_foreground = true,
	},
}
