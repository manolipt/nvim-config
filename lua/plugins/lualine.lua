return {
	'nvim-lualine/lualine.nvim',
	dependencies = {
		"nvim-tree/nvim-web-devicons",
		{
			'lewis6991/gitsigns.nvim',
			config = function()
				require('gitsigns').setup({
					current_line_blame = true,
					current_line_blame_opts = {
						virt_text = true,
						virt_text_pos = 'eol',
					},
					current_line_blame_formatter = '<author> - (<abbrev_sha>) <summary>',
				})
			end,
		},
	},
	config = function()
		local function diff_source()
			local gitsigns = vim.b.gitsigns_status_dict
			if gitsigns then
				return {
					added = gitsigns.added,
					modified = gitsigns.modified,
					removed = gitsigns.removed
				}
			end
		end
		require("lualine").setup({
			icons_enabled = true,
			theme = "catppuccin",
			sections = {
				lualine_b = { 
					{ 'b:gitsigns_head', icon = ''},
					{'diff', source = diff_source},
				},
			}
		})
	end,
}
