return {
	'akinsho/toggleterm.nvim',
	version = "*",
	config = true,
	keys = {
		{ "<leader>t", "<cmd>ToggleTerm<cr>", desc="Toggle Terminal"},
	},
}
