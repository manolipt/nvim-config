vim.g.mapleader = ' '
vim.g.maplocalleader = ' '

vim.keymap.set("i", "jj", "<Esc>")
vim.keymap.set("i", "jk", "<Esc>")

vim.keymap.set("n", "<C-h>", "<c-w><c-h>")
vim.keymap.set("n", "<C-j>", "<c-w><c-j>")
vim.keymap.set("n", "<C-k>", "<c-w><c-k>")
vim.keymap.set("n", "<C-l>", "<c-w><c-l>")

vim.keymap.set("n", "<leader><leader>w", "<cmd>:w<cr>")
vim.keymap.set("n", "<leader>wq", "<cmd>:wq<cr>")
vim.keymap.set("n", "<leader>q", "<cmd>:q<cr>")
vim.keymap.set("n", "<leader><leader>W", "<cmd>:wa<cr>")
vim.keymap.set("n", "<leader>WQ", "<cmd>:wqa<cr>")
vim.keymap.set("n", "<leader>Q", "<cmd>:qa<cr>")
