return {
	{
		"williamboman/mason-lspconfig.nvim",
		dependencies = {
			{
				"williamboman/mason.nvim",
				config = true,
				lazy = false,
			},
			"neovim/nvim-lspconfig",
		},
		config = true,
		opts = {
			ensure_installed = {
				-- General
				"typos_lsp",

				-- Markup
				"jsonls",
				"lemminx",

				-- Scripting
				"bashls",
				"lua_ls",
				"powershell_es",

				-- Programming
				"clangd",
				"omnisharp",
				"rust_analyzer",
			},
			automatic_installation = true,
		},
	},
	"folke/neodev.nvim",
	{
		"hrsh7th/nvim-cmp",
		dependencies = {
			"L3MON4D3/LuaSnip",
			"saadparwaiz1/cmp_luasnip",
			"rafamadriz/friendly-snippets",
			"hrsh7th/cmp-nvim-lsp",
		},
	},
	{
		"nvim-treesitter/nvim-treesitter",
		build = ":TSUpdate",
	}
}
