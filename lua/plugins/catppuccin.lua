return {
	"catppuccin/nvim",
	priority = 1000,
	config = function()
		vim.cmd("colorscheme catppuccin-macchiato")
	end,
	opts = {
		integrations = {
			cmp = true,
			--gitsigns = true,
			neogit=true,
			--nvimtree = true,
			treesitter = true,
			--notify = false,
			--mini = {
			--	enabled = true,
			--	indentscope_color = "",
			--},
		}
	},
}
