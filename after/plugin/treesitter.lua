require('nvim-treesitter.configs').setup {
	ensure_installed = {
		'vim',
		'vimdoc',
		'lua',
		'markdown',
		'markdown_inline',
	},

	auto_install = true,

	highlight = { enable = true },

	indent = { enable = true },
}
