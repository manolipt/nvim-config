local ts = require('telescope')
local builtin = require('telescope.builtin')
ts.setup({
	extensions = {
		fzf = {
			fuzzy = true,
			override_generic_sorter = true,
			override_file_sorter = true,
			case_mode = "smart_case",
		}
	}
})

ts.load_extension('fzf')

vim.keymap.set('n', '<leader>gb', builtin.git_branches)
vim.keymap.set('n', '<leader>gf', builtin.git_files)

vim.keymap.set('n', '<leader>sb', builtin.current_buffer_fuzzy_find)
vim.keymap.set('n', '<leader>sf', builtin.find_files)
vim.keymap.set('n', '<leader>sg', builtin.live_grep)
vim.keymap.set('n', '<leader>so', builtin.treesitter)
vim.keymap.set('n', '<leader>p', builtin.commands)
