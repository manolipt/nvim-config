return {
	"tpope/vim-fugitive",
	lazy = false,
	keys = {
		{ "<leader>gg", "<cmd>Git<cr>", desc="Fugitive Git Status" },
		{ "<leader>gw", "<cmd>Gwrite<cr>", desc="Stage file" },
		{ "<leader>gd", "<cmd>Gdiffsplit<cr>", desc="Git Diff Split View" }
	},
}
